# Vue.JS clean template

A clean template with all the basics you need based on Vue webpack CLI:

- Axios

- Vuex

- SASS loader

- Vue Meta

- Click outside and tooltip directives

- Lodash, moment, font awesome

- Default CSS assets structure

- Default Modal global component

- Global loading/isMobile store properties

- Pre-filled router, store, components and global mixins

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
