const Mixins = {
  install (Vue, options) {
    Vue.mixin({
      methods: {},
      filters: {
        truncate (value, length) {
          if (!length) {
            length = 250
          }
          if (value.length > length) {
            return value.substring(0, length) + '...'
          }
          return value
        }
      },
      computed: {
        user () {
          return this.$store.state.user
        },
        loading () {
          return this.$store.state.loading
        },
        isMobile () {
          return this.$store.state.isMobile
        }
      }
    })
  }
}

export default Mixins
