import Vue from 'vue'
import App from './App'

import VueRouter from 'vue-router'
import Meta from 'vue-meta'

import {routes} from './routes'
import store from './store/store'
import Mixins from './mixins'

// plug
import VTooltip from 'v-tooltip'
import vClickOutside from 'v-click-outside'
import Modal from './components/plugins/Modal.vue'

import './assets/css/main.scss'

Vue.use(VueRouter)
Vue.use(Meta)
Vue.use(VTooltip)
Vue.use(Mixins)

Vue.component('Modal', Modal)

Vue.directive('clickOutside', vClickOutside.directive)

Vue.config.productionTip = false

VTooltip.options.autoHide = true
VTooltip.enabled = window.innerWidth > 1025

const router = new VueRouter({
  mode: 'history',
  routes,
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    return {x: 0, y: 0}
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})
