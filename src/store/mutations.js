export default {
  SET_LOADING (state, bool) {
    state.loading = bool
  },
  SET_MOBILE (state, width) {
    (width < 1025) ? state.isMobile = true : state.isMobile = false
  }
}
