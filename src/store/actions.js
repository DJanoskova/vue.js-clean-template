import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)

axios.defaults.baseURL = `http://localhost:8000`
axios.defaults.withCredentials = true

export default {
  GET_DATA (context) {
    return new Promise((resolve, reject) => {
      Vue.axios.get('/user').then((response) => {
        // context.commit('SET_DATA', response.data)
        resolve(response.data)
      }, error => {
        reject(error.response.data)
      })
    })
  }
}
