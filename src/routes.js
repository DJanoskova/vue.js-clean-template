import Homepage from './components/Homepage'
import UserArea from './components/UserArea'

export const routes = [
  {
    name: 'homepage',
    path: '',
    component: Homepage
  },
  {
    path: '/',
    component: UserArea,
    children: []
  }
]
